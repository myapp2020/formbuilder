<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Module FormBuilder</title>

    
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="adminlts/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="adminlts/dist/css/adminlte.min.css">

  <link rel="stylesheet" href="adminlts/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
</head>

<body>
    <div class="wrapper">
        @yield('content')
    </div>

    <script src="adminlts/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="adminlts/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="adminlts/plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="adminlts/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="adminlts/plugins/moment/moment.min.js"></script>
<script src="adminlts/plugins/inputmask/jquery.inputmask.min.js"></script>
<!-- date-range-picker -->
<script src="adminlts/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="adminlts/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="adminlts/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="adminlts/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- BS-Stepper -->
<script src="adminlts/plugins/bs-stepper/js/bs-stepper.min.js"></script>
<!-- dropzonejs -->
<script src="adminlts/plugins/dropzone/min/dropzone.min.js"></script>
<!-- AdminLTE App -->
<script src="adminlts/dist/js/adminlte.min.js"></script>


<script src="jqueryui/jquery-ui.min.js"></script>


@yield('script')
</body>

</html>