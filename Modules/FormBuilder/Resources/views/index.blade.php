@extends('formbuilder::layouts.master')

@section('content')


<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="index3.html" class="nav-link">Home</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">Contact</a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Navbar Search -->
        <li class="nav-item">
            <a class="nav-link" data-widget="navbar-search" href="#" role="button">
                <i class="fas fa-search"></i>
            </a>
            <div class="navbar-search-block">
                <form class="form-inline">
                    <div class="input-group input-group-sm">
                        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                        <div class="input-group-append">
                            <button class="btn btn-navbar" type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                            <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </li>

        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-comments"></i>
                <span class="badge badge-danger navbar-badge">3</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <a href="#" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                        <img src="dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                        <div class="media-body">
                            <h3 class="dropdown-item-title">
                                Brad Diesel
                                <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                            </h3>
                            <p class="text-sm">Call me whenever you can...</p>
                            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                        </div>
                    </div>
                    <!-- Message End -->
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                        <img src="dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                        <div class="media-body">
                            <h3 class="dropdown-item-title">
                                John Pierce
                                <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                            </h3>
                            <p class="text-sm">I got your message bro</p>
                            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                        </div>
                    </div>
                    <!-- Message End -->
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                        <img src="dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                        <div class="media-body">
                            <h3 class="dropdown-item-title">
                                Nora Silvester
                                <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                            </h3>
                            <p class="text-sm">The subject goes here</p>
                            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                        </div>
                    </div>
                    <!-- Message End -->
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
            </div>
        </li>
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-bell"></i>
                <span class="badge badge-warning navbar-badge">15</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-header">15 Notifications</span>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-envelope mr-2"></i> 4 new messages
                    <span class="float-right text-muted text-sm">3 mins</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-users mr-2"></i> 8 friend requests
                    <span class="float-right text-muted text-sm">12 hours</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-file mr-2"></i> 3 new reports
                    <span class="float-right text-muted text-sm">2 days</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                <i class="fas fa-th-large"></i>
            </a>
        </li>
    </ul>
</nav>
<!-- /.navbar -->

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Alexander Pierce</a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <li class="nav-item menu-open">
                    <a href="#" class="nav-link active">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Starter Pages
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="#" class="nav-link active">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Active Page</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Inactive Page</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Simple Link
                            <span class="right badge badge-danger">New</span>
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Form Builder</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Starter Page</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">

                <div class="col-2">
                    <button type="button" class="btn btn-block bg-gradient-success" id="text">Add Text</button>
                </div>
                <div class="col-2">
                    <button type="button" class="btn btn-block bg-gradient-success" id="textarea">Add Textarea</button>
                </div>
                <div class="col-2">
                    <button type="button" class="btn btn-block bg-gradient-success" id="combo">Add Combo</button>
                </div>
                <div class="col-2">
                    <button type="button" class="btn btn-block bg-gradient-success" id="datepicker">Add Date picker</button>
                </div>

                <div class="col-2">
                    <button type="button" class="btn btn-block bg-gradient-success">Add Radio</button>
                </div>
                <div class="col-2">

                </div>

            </div>

            <section  id="form_container">

            </section>
            <button type="button" class="btn btn-info mt-5" id="save_form">Save form</button>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
    </div>
</aside>
<!-- /.control-sidebar -->

<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
        Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
</footer>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        var el_cou = 0;

        $('#text').click(function() {
            el_cou++;
            $("#form_container").append(
                    '<div class="row fb-single-element-wrapper" data-type="text">\
                        <div class="col-sm-6 fb-text-con">\
                            <div class="form-group mt-2">\
                                <label>Label_'+ el_cou +'</label>\
                                <input type="text" class="form-control fb-text fb-element" name="name_'+ el_cou +'" placeholder="">\
                            </div>\
                        </div>\
                        <div class="col-sm-6 fb-text-options fb-options" style="display:none">\
                            <div class="card mt-2" style="width: 18rem;">\
                                <div class="card-body">\
                                    <h6 class="card-subtitle mb-2 text-muted">Options</h6>\
                                    <div class="card-body">\
                                        <div class="row border p-2 mt-2 element-name-con">\
                                            <div class="col">\
                                                <label>Name</label>\
                                                <input type="text" class="form-control form-control-sm"  placeholder="field name">\
                                            </div>\
                                        </div>\
                                        <div class="row border p-2 mt-2 element-label-con">\
                                            <div class="col">\
                                                <label>Label</label>\
                                                <input type="text" class="form-control form-control-sm" placeholder="field label">\
                                            </div>\
                                        </div>\
                                        <div class="form-check form-check-inline">\
                                            <input class="form-check-input validations" type="checkbox" id="inlineCheckbox1" value="required">\
                                            <label class="form-check-label" for="inlineCheckbox1">required</label>\
                                        </div>\
                                        <div class="form-check form-check-inline">\
                                            <input class="form-check-input validations" type="checkbox" id="inlineCheckbox2" value="number">\
                                            <label class="form-check-label" for="inlineCheckbox2">number</label>\
                                        </div>\
                                        <div class="form-check form-check-inline">\
                                            <input class="form-check-input validations" type="checkbox" id="inlineCheckbox3" value="date">\
                                            <label class="form-check-label" for="inlineCheckbox3">date</label>\
                                        </div>\
                                        <button type="button" class="btn btn-primary btn-sm mt-2 add-btn" data-type="text">Add</button>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>');
        });

        $('#textarea').click(function() {
            $("#form_container").append('<div class="col-sm-6">\
                      <div class="form-group">\
                        <label>Textarea</label>\
                        <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>\
                      </div>\
                    </div> <div class="col-sm-6"></div>');
        });

        $('#combo').click(function() {
            $("#form_container").append('    <div class="col-sm-6">\
                      <div class="form-group">\
                        <label>Select</label>\
                        <select class="form-control">\
                          <option>option 1</option>\
                          <option>option 2</option>\
                          <option>option 3</option>\
                          <option>option 4</option>\
                          <option>option 5</option>\
                        </select>\
                      </div>\
                    </div> <div class="col-sm-6"></div>');
        });

        $('#datepicker').click(function() {
            $("#form_container").append('<div class="col-sm-6">\
            <div class="form-group">\
                  <label>Date:</label>\
                    <div class="input-group date" id="reservationdate" data-target-input="nearest">\
                        <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate"/>\
                        <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">\
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>\
                        </div>\
                    </div>\
                </div>\
                    </div> <div class="col-sm-6"></div>');
        });

        $( "#form_container" ).delegate( ".fb-element", "focus", function() {
            $('.fb-options').hide();
            $(this).parent().parent().siblings('.fb-options').first().show()
        
        });

        $( "#form_container" ).delegate( ".add-btn", "click", function() {
           //et - elememt type
            type = $(this).data('type')

            if(type=='text'){
                name  =  $(this).parent().children('.element-name-con').find('input').val()
                label  =  $(this).parent().children('.element-label-con').find('input').val()

                textbox = $(this).parent().parent()
                .parent().parent()
                .siblings('.fb-text-con')
                .find('.fb-text').first()

                textbox_label = $(this).parent().parent()
                .parent().parent()
                .siblings('.fb-text-con')
                .find('label').first()
                
                textbox.attr('name', name)
                textbox_label.html(label)
            }
        });

        $('#save_form').click(function(){
            validation = {};
            $('.fb-single-element-wrapper').each(function(){
                element = $(this).data('type')
                name = $(this).find('.fb-element').first().attr('name');
                label = $(this).find('label').first().html()

               $(this).find('.fb-options').find('.validations:checked').each(function(){
                  validation = $(this).val()
               })
                console.log(element+ '\n'  + name + '\n' + label + '\n' + validation);
            });
           
        });
    });
</script>

<script>
    $(function() {
        $("#form_container").sortable();
        $('#reservationdate').datetimepicker({
            format: 'L'
        });

    });
</script>

@endsection